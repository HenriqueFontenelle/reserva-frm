##library(RMySQL) # will load DBI as well
library(RODBC)
library(data.table)
library(plyr)
library("ggplot2")

normalize <- function(x) {
  resp <- ((x - min(x)) / (max(x) - min(x)))
  
  return (resp)
}

exagerados <- function(x,maxFreq) {
  if(x > maxFreq){
    return(maxFreq)
  }else{
    return(x)
  }
}

thirdDimention <- function(x,)

data_Inicial <- '\'2020-01-01\''
data_Final <-'\'2020-02-12\''
tempoAteInativo <- 366

con <- odbcDriverConnect(
  "Driver={ODBC Driver 17 for SQL Server}; Server=tcp:10.10.0.10; Database=Olap-Reserva; UID=usr_r; Pwd=!@usr199421r#;"
)
## open a connection to a MySQL database
##con <-dbConnect(MySQL(), user ='lintpedidos', password = 'mdblsdh$$*#mndm', dbname = 'Olap-Reserva', host = '10.10.0.10')
## list the tables in the database
##dbListTables(con)
#userData <- dbGetQuery(con,'select * from dbo.TMP_LENTE_VENDA')

#query <- paste0("select cpf_cliente, count(data_venda_hora) as qtd_vezes_voltou,sum(qtde_venda) as qtd_de_produtos , DATEDIFF (day, MAX(data_venda),getdate()) as ultima_compra_dia , avg(valor_venda) as avg_gasto_ultimo_ano
#                    from rsv_olap_lente_venda
#                    where qtde_venda > 0 and griffe = 'RESERVA'
#                    group by cpf_cliente
#                    having count(data_venda_hora) > 1 and count(data_venda_hora) < 120 and DATEDIFF (day, MAX(data_venda),getdate()) < 366
#                    order by DATEDIFF (day, MAX(data_venda),getdate()) , count(data_venda_hora) desc , sum(qtde_venda) desc")

query <- "select		cpf_cliente, COUNT (DISTINCT ticket) qtd_vezes_voltou ,sum(qtde_venda) as qtd_de_produtos , DATEDIFF (day, MAX(data_venda),getdate()) as ultima_compra_dia , avg(valor_venda) as avg_gasto_ultimo_ano ,  sum(valor_venda) as total_gasto_ultimo_ano
          from		rsv_olap_lente_venda
          where		qtde_venda > 0 and griffe = 'RESERVA'
          group by	cpf_cliente
          having		DATEDIFF (day, MAX(data_venda),getdate()) < 366 and COUNT (DISTINCT ticket) between 2 and 119
          order by	DATEDIFF (day, MAX(data_venda),getdate()) , COUNT (DISTINCT ticket) desc , sum(qtde_venda) desc"


tempUserData <- sqlQuery(con, query)

na.omit(tempUserData)

userData <- data.table()
userData$freq <- tempUserData$qtd_vezes_voltou
userData$rec <- tempUserData$ultima_compra_dia
userData$mon <- tempUserData$total_gasto_ultimo_ano

na.omit(userData)

mediaFreqTot <- mean(userData$freq)
mediaFreqTot

mediaRecTot <- mean(userData$rec)
mediaRecTot

#Esta funcao eh o que diferencia um usuario Usual e usuario elite.
limitanteFreq <- function(x) x * 2

#Pego a frequencia maxima utilizando a funcao limitanteFreq, e arredondo pra cima.
#Criando a diferença entre o Usual e o Elite
maxFreq <- ceiling(limitanteFreq(mediaFreqTot))
maxFreq

usualUsers <- data.table()
usualUsers <- userData[userData$freq <= maxFreq,] #Aqui falo que os usuals sao os que tiverem a frequencia < ou = maxFreq
usualUsers

mediaFreqUsual <- mean(usualUsers$freq)

mediaRecUsual <- mean(usualUsers$rec)

eliteUsers <- data.table()
eliteUsers <- userData[userData$freq > maxFreq,] #Aqui é o contrario do usual
eliteUsers

mediaFreqElite <- mean(eliteUsers$freq)

mediaRecElite <- mean(eliteUsers$rec)


dados <- file("Dadosmedios.txt")
writeLines(c("Media Frequencia Total",mediaFreqTot,"Media Recencia Total",mediaRecTot,"Media Frequencia Usual",mediaFreqUsual,"Media Recencia Usual",mediaRecUsual,"Media Frequencia Elite",mediaFreqElite,"Media Recencia Elite",mediaRecElite),dados)
close(dados)


#userData$freq <- lapply(userData$freq,exagerados)
#userData

##############################################
##Ocorrencias de Frequencia
ocurFreqUsual<-table(usualUsers$freq)
ocurFreqElite<-table(eliteUsers$freq)

pdf(file="OcorrenciasFrequencias.pdf")

barplot(ocurFreqUsual,main="Ocorrencia Frequencia Usuarios Normais")
barplot(ocurFreqElite,main="Ocorrencia Frequencia Usuarios Elite")

dev.off()

png(file="OcorrenciasFrequenciasNormais.png")

barplot(ocurFreqUsual,main="Ocorrencia Frequencia Usuarios Normais")

dev.off()

png(file="OcorrenciasFrequenciasElite.png" )

barplot(ocurFreqElite,main="Ocorrencia Frequencia Usuarios Elite")

dev.off()

############################################
##Ocorrencias de recencia
ocurReqUsual<-table(usualUsers$rec)
ocurReqElite<-table(eliteUsers$rec)

find(max(ocurReqUsual),numeric = true)

max(ocurReqUsual)
ocurReqUsual
max(ocurReqElite)
ocurReqElite

pdf(file="OcorrenciasRecencia.pdf")

barplot(ocurReqUsual)
barplot(ocurReqElite)

dev.off()

png(file="OcorrenciasRecenciaNormais.png")

barplot(ocurReqUsual)

dev.off()

png(file="OcorrenciasRecenciaElite.png")

barplot(ocurReqElite)

dev.off()

################################################################
## Frequencia X Recencia

pdf(file="FrequenciaXRecencia.pdf")

#plot(x=normUserData$recencia, y=normUserData$frequencia, xlab = "recencia", ylab = "frequencia",xlim = c(1,0),main = "Normalized User Data",pch=20 )
plot(x=usualUsers$rec,y=usualUsers$freq, xlab = "recencia", ylab = "frequencia",main = "Usual User Data",pch=20)
plot(x=eliteUsers$rec,y=eliteUsers$freq, xlab = "recencia", ylab = "frequencia",main = "Elite User Data",pch=20)

dev.off()

#####################################################

normOcurUsual <- data.table()
normOcurUsual$freq <- normalize(usualUsers$freq) 
normOcurUsual$rec <- normalize(usualUsers$rec) 

na.omit(normOcurUsual)

normOcurElite <- data.table()
normOcurElite$freq <- normalize(eliteUsers$freq) 
normOcurElite$rec <- normalize(eliteUsers$rec) 

na.omit(normOcurElite)

pdf(file="FrequenciaXRecenciaNormalizado.pdf")

#plot(x=normUserData$recencia, y=normUserData$frequencia, xlab = "recencia", ylab = "frequencia",xlim = c(1,0),main = "Normalized User Data",pch=20 )
plot(x=normOcurUsual$rec,y=normOcurUsual$freq, xlab = "recencia", ylab = "frequencia",xlim = c(1,0),main = "Usual User Data Normalized",pch=20)
plot(x=normOcurElite$rec,y=normOcurElite$freq, xlab = "recencia", ylab = "frequencia",xlim = c(1,0),main = "Elite User Data Normalized",pch=20)

dev.off()

odbcClose(con)