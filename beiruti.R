install.packages("treemap")
library(treemap)

group=c(rep("grupo1",4), rep("grupo2",2), rep("grupo3",4))
subgroup=paste("subgrupo",c(1,2,3,4,1,2,1,2,3,4), sep="-")
value = c(23,5,22,12,11,7,3,5,23,12)
data=data.frame(group,subgroup,value)

treemap(data, index=c("group","subgroup"),
        vSize= "value", type="index",
        fontsize.labels=c(20,10),
        fontcolor.labels=c("black","white"),
        fontface.labels = c(3,2),
        bg.labels=c("transparent"),
        align.labels = list( c("center","center"), c("right","bottom"))
       
        
)

