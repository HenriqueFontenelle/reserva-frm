select cpf_cliente, count(data_venda_hora) as qtd_vezes_voltou,sum(qtde_venda) as qtd_de_produtos , DATEDIFF (day, MAX(data_venda),getdate()) as ultima_compra_dia , avg(valor_venda) as avg_gasto_ultimo_ano
                    from rsv_olap_lente_venda
                    where qtde_venda > 0 and griffe = 'RESERVA'
                    group by cpf_cliente
                    having count(data_venda_hora) > 3 and count(data_venda_hora) < 120 and DATEDIFF (day, MAX(data_venda),getdate()) < 366
                    order by DATEDIFF (day, MAX(data_venda),getdate()) , count(data_venda_hora) desc , sum(qtde_venda) desc